package com.example.analyse_besoin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnalyseBesoinApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnalyseBesoinApplication.class, args);
	}

}
