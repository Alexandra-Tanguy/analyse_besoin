package com.example.analyse_besoin;

import java.time.LocalTime;

public interface IHorloge {

    LocalTime now();
}
