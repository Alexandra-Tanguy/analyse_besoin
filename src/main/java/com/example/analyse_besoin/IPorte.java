package com.example.analyse_besoin;

public interface IPorte {
    public void ouvrir();
    public boolean isBlocked();
}
