package com.example.analyse_besoin;

public interface IMoteurOuvertureLogger {
    void logException(Exception e);
}

