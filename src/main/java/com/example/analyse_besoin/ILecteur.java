package com.example.analyse_besoin;

public interface ILecteur {
    IPorte getPorte();
    boolean isBadgeDetecte();

}

