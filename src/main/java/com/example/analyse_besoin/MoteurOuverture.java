package com.example.analyse_besoin;

import java.time.LocalTime;

public class MoteurOuverture {
    private final ILecteur lecteurAPoller;
    private IMoteurOuvertureLogger logger;
    private boolean autorisationOuverture = false;

    public MoteurOuverture(ILecteur lecteurAPoller) {
        this.lecteurAPoller = lecteurAPoller;
    }

    public void setLogger(IMoteurOuvertureLogger logger) {
        this.logger = logger;
    }



    public void interrogerLecteurs() {
        try {
            if (lecteurAPoller.isBadgeDetecte() && !lecteurAPoller.getPorte().isBlocked()) {
                lecteurAPoller.getPorte().ouvrir();
            }
        } catch (Exception e) {
            if (logger != null) {
                logger.logException(e);
            }
        }
    }

//    public void interrogerLecteursAvecHoraire(LocalTime heureActuelle) {
//        try {
//            if (lecteurAPoller.isBadgeDetecte() && !lecteurAPoller.getPorte().isBlocked() && verifierHoraire(heureActuelle)) {
//                lecteurAPoller.getPorte().ouvrir();
//            }
//        } catch (Exception e) {
//            if (logger != null) {
//                logger.logException(e);
//            }
//        }
//    }

}