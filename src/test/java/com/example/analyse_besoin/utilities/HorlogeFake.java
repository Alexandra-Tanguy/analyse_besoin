package com.example.analyse_besoin.utilities;

import com.example.analyse_besoin.IHorloge;

import java.time.LocalTime;

public class HorlogeFake implements IHorloge {

    private final LocalTime heureFixe;
    public HorlogeFake(LocalTime heureFixe) {
        this.heureFixe = heureFixe;
    }

    @Override
    public LocalTime now() {
        return heureFixe;
    }
}
