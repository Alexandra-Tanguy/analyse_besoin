package com.example.analyse_besoin.utilities;

import com.example.analyse_besoin.IMoteurOuvertureLogger;

public class LoggerSpy implements IMoteurOuvertureLogger {
    private boolean exceptionDansLesLogs = false;

    @Override
    public void logException(Exception e) {
        exceptionDansLesLogs = true;
    }

    public boolean isExceptionDansLesLogs() {
        return exceptionDansLesLogs;
    }
}
