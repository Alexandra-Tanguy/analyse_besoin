package com.example.analyse_besoin.utilities;

import com.example.analyse_besoin.IHorloge;
import com.example.analyse_besoin.IPorte;
import com.example.analyse_besoin.MoteurOuverture;

import java.time.LocalTime;

public class MoteurOuvertureBuilder {

    private IPorte _porte;

    private IHorloge _horloge;
    private boolean _detecteUnBadge;
    private boolean horaireOuverture = false;

    public MoteurOuvertureBuilder() {
    }

    public MoteurOuverture build() {
        LecteurFake lecteur = new LecteurFake(_porte);
        if (_detecteUnBadge) {
            lecteur.SimulerDetectionBadge();
        }

        return new MoteurOuverture(lecteur);
    }

    public MoteurOuverture buildHoraire() {
        verifierHoraire();
        LecteurFake lecteur = new LecteurFake(_porte);
        if (_detecteUnBadge && horaireOuverture) {
            lecteur.SimulerDetectionBadge();
        }

        return new MoteurOuverture(lecteur);
    }

    public MoteurOuvertureBuilder espionnantLaPorte(PorteSpy spy) {
        this._porte = spy;
        return this;
    }

    public MoteurOuvertureBuilder detectantUnBadge() {
        this._detecteUnBadge = true;
        return this;
    }

    public MoteurOuvertureBuilder detecteHoraire(HorlogeFake horlogeFake) {
        this._horloge = horlogeFake;
        return this;
    }

    public MoteurOuvertureBuilder verifierHoraire() {
        LocalTime heureDebut = LocalTime.of(0, 0); // 00h00
        LocalTime heureFin = LocalTime.of(1, 0); // 1h00
        LocalTime heureActuelle = _horloge.now();

        if (!heureActuelle.isBefore(heureDebut) && heureActuelle.isBefore(heureFin)) {
            this.horaireOuverture = true;
        }
        return this;
    }
}
