package com.example.analyse_besoin.utilities;
import com.example.analyse_besoin.IPorte;

public class PorteSpy implements IPorte {
    private final IPorte decorated;

    private boolean ARecuUnSignalDOuverture = false;

    public PorteSpy(IPorte decorated) {
        this.decorated = decorated;
   }

    @Override
    public void ouvrir() {
        this.ARecuUnSignalDOuverture = true;
        decorated.ouvrir();
    }



    @Override
    public boolean isBlocked() {
        return this.decorated.isBlocked();
    }

    // getter
    public boolean RecuUnSignalDOuverture() {
        return ARecuUnSignalDOuverture;
    }
}

