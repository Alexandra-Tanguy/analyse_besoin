package com.example.analyse_besoin.utilities;

import com.example.analyse_besoin.ILecteur;
import com.example.analyse_besoin.IPorte;

public class LecteurFake implements ILecteur {
    private IPorte porte;
    public boolean badgeDetecteAuProchainCycle;
    private boolean lecteurDefaillant;

    public LecteurFake(IPorte porte) {
        this.porte = porte;
    }

    public void SimulerDetectionBadge() {
        badgeDetecteAuProchainCycle = true;
    }
    public void LecteurIsDefaillant() {
        lecteurDefaillant = true;
    }

    @Override
    public IPorte getPorte() {
        return porte;
    }

    @Override
    public boolean isBadgeDetecte() {
        if (!badgeDetecteAuProchainCycle) {
            return false;
        }
        badgeDetecteAuProchainCycle = false;
        return true;
    }

    public boolean isDefaillant() {
        if (!lecteurDefaillant) {
            return false;
        }

        return true;
    }

}
