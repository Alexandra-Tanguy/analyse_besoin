package com.example.analyse_besoin.utilities;

import com.example.analyse_besoin.ILecteur;
import com.example.analyse_besoin.IPorte;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


public class LecteurDummy implements ILecteur {
    @Override
    public IPorte getPorte() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isBadgeDetecte() {
        throw new UnsupportedOperationException();
    }
}

