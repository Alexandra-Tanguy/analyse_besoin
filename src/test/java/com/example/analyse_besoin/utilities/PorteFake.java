package com.example.analyse_besoin.utilities;

import com.example.analyse_besoin.IPorte;

public class PorteFake implements IPorte {

    private boolean isBlocked;

    public PorteFake(boolean isBlocked) {
        this.isBlocked = isBlocked;
    }

    @Override
    public void ouvrir() {}

    @Override
    public boolean isBlocked() {
        return this.isBlocked;
    }

}
