package com.example.analyse_besoin;

import com.example.analyse_besoin.utilities.HorlogeFake;
import com.example.analyse_besoin.utilities.MoteurOuvertureBuilder;
import com.example.analyse_besoin.utilities.PorteFake;
import com.example.analyse_besoin.utilities.PorteSpy;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class TestMoteurOuverture {

    @Test
    public void casNominal() {
        // ETANT DONNE un Lecteur relié à une porte
        // ET un moteur d'ouverture interrogeant ce lecteur
        PorteSpy porteSpy = new PorteSpy(new PorteFake(false));
        MoteurOuvertureBuilder moteurBuilder = new MoteurOuvertureBuilder()
                .espionnantLaPorte(porteSpy)
                .detectantUnBadge();
        MoteurOuverture moteur = moteurBuilder.build();

        // Quand un badge valide passe devant le lecteur
        moteur.interrogerLecteurs();

        // ALORS un signal d'ouverture est envoyé à la porte
        assertTrue(porteSpy.RecuUnSignalDOuverture());
    }


    @Test
    public void casRienDetecte() {
        // ETANT DONNE un Lecteur relié à une porte
        // ET un moteur d'ouverture interrogeant ce lecteur
        PorteSpy porteSpy = new PorteSpy(new PorteFake(false));
        MoteurOuvertureBuilder moteurBuilder = new MoteurOuvertureBuilder()
                .espionnantLaPorte(porteSpy);
        MoteurOuverture moteur = moteurBuilder.build();

        //Quand aucun badge n'est présenté au lecteur
        moteur.interrogerLecteurs();

        //ALORS un signal d'ouverture est envoyé à la porte
        assertFalse(porteSpy.RecuUnSignalDOuverture());
    }

    @Test
    public void porteBloquee() {
        // Étant donné un Lecteur est relié à une Porte bloquée
        PorteSpy porteSpy = new PorteSpy(new PorteFake(true));

        // Et un moteur d’ouverture interroge ce Lecteur
        // Quand on présente un badge
        MoteurOuvertureBuilder moteurBuilder = new MoteurOuvertureBuilder()
                .espionnantLaPorte(porteSpy)
                .detectantUnBadge();
        MoteurOuverture moteur = moteurBuilder.build();

        moteur.interrogerLecteurs();
        // Alors le moteur d’ouverture n’autorise pas l’ouverture de la Porte
        // Et aucun signal d’ouverture n’est envoyé à la porte
        assertFalse(porteSpy.RecuUnSignalDOuverture());
    }

    @Test
    public void porteAvecHoraireAutorisee() {
        // Étant donné un Lecteur reliée à une Porte ouverte de 00h à 1h
        PorteSpy porteSpy = new PorteSpy(new PorteFake(false));

        // Et un moteur d’ouverture interrogeant ce Lecteur
        // Quand un badge valide est passé devant ce Lecteur à 00h30
        MoteurOuvertureBuilder moteurBuilder = new MoteurOuvertureBuilder()
                .detecteHoraire(new HorlogeFake(LocalTime.of(0, 30)))
                .espionnantLaPorte(porteSpy)
                .detectantUnBadge()
                .verifierHoraire();
        MoteurOuverture moteur = moteurBuilder.buildHoraire();

        moteur.interrogerLecteurs();

        // ALORS un signal d'ouverture est envoyé à la porte
        assertTrue(porteSpy.RecuUnSignalDOuverture());
    }

    @Test
    public void porteHoraireNonAutorisee() {
        // Étant donné un Lecteur reliée à une Porte ouverte de 00h à 1h
        PorteSpy porteSpy = new PorteSpy(new PorteFake(false));

        // Et un moteur d’ouverture interrogeant ce Lecteur
        // Quand un badge valide est passé devant ce Lecteur à 23h59
        MoteurOuvertureBuilder moteurBuilder = new MoteurOuvertureBuilder()
                .detecteHoraire(new HorlogeFake(LocalTime.of(23, 59)))
                .espionnantLaPorte(porteSpy)
                .detectantUnBadge()
                .verifierHoraire();
        MoteurOuverture moteur = moteurBuilder.buildHoraire();

        moteur.interrogerLecteurs();

        // ALORS aucun signal d’ouverture n’est envoyé à la porte
        assertFalse(porteSpy.RecuUnSignalDOuverture());
    }

    // Test pour vérifier que la porte n’autorise pas l’ouverture après les horaires autorisées
    @Test
    public void porteHoraireNonAutoriseeApresHeureFermeture() {
        // Étant donné un Lecteur reliée à une Porte ouverte de 00h à 1h
        PorteSpy porteSpy = new PorteSpy(new PorteFake(false));

        // Et un moteur d’ouverture interrogeant ce Lecteur
        // Quand un badge valide est passé devant ce Lecteur à 1h10
        MoteurOuvertureBuilder moteurBuilder = new MoteurOuvertureBuilder()
                .detecteHoraire(new HorlogeFake(LocalTime.of(1, 1)))
                .espionnantLaPorte(porteSpy)
                .detectantUnBadge()
                .verifierHoraire();
        MoteurOuverture moteur = moteurBuilder.buildHoraire();

        moteur.interrogerLecteurs();

        // ALORS aucun signal d’ouverture n’est envoyé à la porte
        assertFalse(porteSpy.RecuUnSignalDOuverture());
    }


}
